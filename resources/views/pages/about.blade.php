@extends('layouts.app')
@section('content')
<h1>Images Gallery</h1>

@if(count($posts) > 0) 
<div class="row col-md-12">
@foreach($posts as $post)
@if($post->cover_image != "noimage.jpg")
<img class="img-thumbnail" style="width:200px;height:150px;" src="/storage/cover_images/{{$post->cover_image}}" />
@endif 
@endforeach 
</div>
@else
<p>No image found</p>
@endif 

@endsection

