@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-deflaut">Go Back</a>

    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            <h1 class="panel-title">{{$post->title}}</h1>
        </div>
        <div class="panel-body">
            <div class="text-center">
                <img style="width:50%" src="/storage/cover_images/{{$post->cover_image}}"/>
                <br><br>
                {!!$post->body!!} <!-- !! parse le HTML pour nous -->
            </div>
        @if(!Auth::guest())
                @if(Auth::user()->id == $post->user_id)
                <a href="/posts/{{$post->id}}/edit" class="btn btn-info">Edit</a>

                {!!Form::open(['action' => ['PostController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                {!!Form::close()!!}
            @endif
        @endif
        </div>
        <div class="panel-footer">
            <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
        </div>
    </div>
@endsection