<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    public function index()
    {
        $title = 'Welcome to laravel!';
        return view('pages.index')->with('title', $title);
    }

    public function about()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('pages.about')->with('posts', $posts);
    }

    public function services()
    {
        $data = array(
            'title' => 'Welcome to page of services',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}
